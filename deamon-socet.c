#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <signal.h>
#include <string.h>
#include <errno.h>

#define FILE_PATH "config.txt"
#define SOCKET_PATH "test_socket.sock"


// Создания демона
void daemonize() {
    pid_t pid = fork();
    if (pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    if (pid > 0)
        exit(EXIT_SUCCESS); // Завершаем родительский процесс

    if (setsid() < 0) // Создаем новый сеанс
        exit(EXIT_FAILURE);

    // Дополнительная форковая операция, чтобы избежать возникновения tty
    pid = fork();
    if (pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    if (pid > 0)
        exit(EXIT_SUCCESS);

    umask(0); // Устанавливаем разрешения файлов

    // Перенаправление стандартных файловых дескрипторов
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

// Получения размера файла
off_t get_file_size(const char *filename) {
    struct stat st;
    if (stat(filename, &st) == -1) {
        perror("stat");
        return -1; // Ошибка доступа к файлу
    }
    return st.st_size; // Возвращает размер файла в байтах
}

// Работы с Unix domain socket
void run_server(const char *socket_path, const char *file_path) {
    int server_fd, client_fd;
    struct sockaddr_un server_addr;
    socklen_t server_len = sizeof(server_addr);

    // Удаление предыдущего сокета, если он существует
    unlink(socket_path);

    // Создание сокета для прослушивания
    if ((server_fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // Настройка параметров адреса сокета
    memset(&server_addr, 0, server_len);
    server_addr.sun_family = AF_UNIX;
    strncpy(server_addr.sun_path, socket_path, sizeof(server_addr.sun_path) - 1);

    // Привязка сокета к адресу
    if (bind(server_fd, (struct sockaddr *)&server_addr, server_len) == -1) {
        perror("bind");
        close(server_fd);
        exit(EXIT_FAILURE);
    }

    // Начало прослушивания входящих соединений
    if (listen(server_fd, 5) == -1) {
        perror("listen");
        close(server_fd);
        exit(EXIT_FAILURE);
    }

    while (1) {
        // Ожидание клиентских подключений
        if ((client_fd = accept(server_fd, NULL, NULL)) == -1) {
            perror("accept");
            continue;
        }

        // Получение размера файла
        off_t size = get_file_size(file_path);
        char size_str[20];
        // Если не удалось получить размер файла, отправить сообщение об ошибке
        if (size == -1) {
            strcpy(size_str, "error");
        } else {
            snprintf(size_str, sizeof(size_str), "%ld", size);
        }

        // Отправление размера файла клиенту
        if (send(client_fd, size_str, strlen(size_str), 0) == -1) {
            perror("send");
        }

        // Закрытие соединения с клиентом
        close(client_fd);
    }
}

int main(int argc, char *argv[]) {
    if (argc > 1 && strcmp(argv[1], "-d") == 0) {
        daemonize();
    }

    // Запуск сервера для прослушивания запросов на размер файла
    run_server(SOCKET_PATH, FILE_PATH);

    // Сервер завершил свою работу
    return 0;
}

